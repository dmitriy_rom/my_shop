class FeedbackMailer < ApplicationMailer
  default template_path: 'mailers'

  def order_create_email(order)
    @order = order
    mail to: "romanenko.dmitriy.97@mail.ru",
         subject: "Сделан заказ на сайте"
  end

end
