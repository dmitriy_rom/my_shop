ActiveAdmin.register News do
  permit_params :title, :image, :text

  index do
    column :title
    column :image do |news|
      image_tag(news.image.url, width: 150, height: 150)
    end
    column :text
    actions
  end

  show do
    attributes_table do
      row :title
      row :image do |news|
        span image_tag(news.image.url)
      end
      row :text
    end
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :image
      f.input :text
    end
    f.actions
  end
end



