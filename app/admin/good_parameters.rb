ActiveAdmin.register GoodParameter do
  permit_params :title

  index do
    column :title
    actions
  end

  show do
    attributes_table do
      row :title
    end
  end

  form do |f|
    f.inputs do
      f.input :title
    end
    f.actions
  end
end



