ActiveAdmin.register GoodParameterVariant do
  permit_params :title, :good_parameter_id

  index do
    column :title
    column :good_parameter
    actions
  end

  show do
    attributes_table do
      row :title
      row :good_parameter
    end
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :good_parameter, as: :select
    end
    f.actions
  end
end



