ActiveAdmin.register Good do
  permit_params :title, :cost, :description, :prior,
                category_ids: [],
                good_images_attributes: [:image, :id, :image_cache, :_destroy],
                rests_attributes: [:rest, :cost, :_destroy, :id,
                                  good_parameter_variant_ids: [],]

  show do
    column :title
    column :description
    column :categories do |good|
      span do
        good.categories.each do |category|
          li "#{category.title}"
        end
      end
    end
    column :cost
    column :prior
    column :image do |good|
      if good.good_images.any?
        image_tag(good.good_images.first.image.url, width: 150, height: 150)
      end
    end
    actions
  end

  show do
    attributes_table do
      row :title
      row :description
      row :categories do |good|
        good.categories.each do |category|
          li "#{category.title}"
        end
      end
      row :cost
      row :prior
      row :image do |good|
        if good.good_images.any?
          good.good_images.each do |good_image|
            li image_tag(good_image.image.url)
          end
        end
      end
      row :rest do |good|
        if good.rests.any?
          span do
            good.rests.each do |rest|
              li "#{rest.collect_config}, остаток: #{rest.rest}, цена: #{rest.cost}"
            end
          end
        end
      end
      row :description
    end
  end

  form do |f|
    f.inputs do
      f.input :categories, collection: Category.all
      f.input :title
      f.input :description
      f.input :cost
      f.input :prior, hint: "Чем больше значение, тем раньше в списке"
      f.has_many :good_images, allow_destroy: true do |image|
        image.input :image, hint:
            [ "Допустимые форматы: jpg, png, jpeg, gif, svg",
            (image.object.image.present?) ? "<br>Текущее изображение:<br>#{image_tag(image.object.image.url)}" : ""
            ].join.html_safe
        image.input :image_cache, as: :hidden
      end
      f.has_many :rests, allow_destroy: true do |rest|
        rest.input :good_parameter_variants, collection: GoodParameterVariant.all.map do |gpv|
          ["#{gpv.good_parameter.title}: #{gpv.title}", gpv.id]
        end
        rest.input :rest
        rest.input :cost
      end
    end
    f.actions
  end
end



