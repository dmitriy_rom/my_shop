ActiveAdmin.register Category do
  permit_params :title, :slug, :prior

  show do
    column :title
    column :slug
    column :prior
    actions
  end

  show do
    attributes_table do
      row :title
      row :slug
      row :prior
    end
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :slug
      f.input :prior
    end
    f.actions
  end
end



