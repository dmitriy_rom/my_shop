class CartsController < ApplicationController
  def show
    @cart = get_client_cart
    @cart_goods = @cart.cart_goods
    @price = get_cart_price(@cart)
  end

  def toggle_good
    @rest = Rest.find(params[:rest])
    cart = get_client_cart
    rest_in_cart = rest_in_cart?(@rest.id)
    rest_in_cart ?
        CartGood.find_by(rest: @rest, cart: cart).destroy :
        CartGood.create(rest: @rest, cart: cart, count: 1, max_count: @rest.rest)
    return render json: { success: true, in_cart: !rest_in_cart, cart_price: get_cart_price(cart) }
  end

  def rest_change
    @rest = Rest.find(params[:id])
    return render json: { success: true, cost: @rest.cost, in_cart: rest_in_cart?(@rest.id) }
  end

end