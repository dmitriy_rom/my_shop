class ApplicationController < ActionController::Base
  helper_method :rest_in_cart?
  def get_client_cart
    cookies[:makeshift_id] ||= generate_makeshift_id
    if client_signed_in?
      client = current_client
      return client.cart || Cart.find_and_update(cookies[:makeshift_id], client) || Cart.create(client: client)
    else
      return Cart.find_by(makeshift_id: cookies[:makeshift_id]) || Cart.create(makeshift_id: cookies[:makeshift_id])
    end
  end

  def rest_in_cart?(id)
    cart = get_client_cart
    cart.cart_goods.pluck(:rest_id).include?(id)
  end

  def generate_makeshift_id
    loop do
      token = SecureRandom.hex(10)
      break token unless Cart.where(makeshift_id: token).exists?
    end
  end

  def get_cart_price(cart)
    Rest.joins(:cart_goods).where(cart_goods: { id: cart.cart_goods.pluck(:id) }).pluck(:cost).sum
  end
end
