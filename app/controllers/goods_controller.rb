class GoodsController < ApplicationController
  def show
    @good = Good.find_by(slug: params[:slug])
    @rests = @good.rests
    @images = @good.good_images
    @comments = @good.comments
  end
end
