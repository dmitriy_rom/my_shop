class OrdersController < ApplicationController
  def create
    unless client_signed_in?
      redirect_to new_client_session_path and return
    end
    cart = get_client_cart
    if cart.cart_goods.count < 1
      flash[:error] = order
      redirect_to cart_path and return
    end
    order = Order.new(client: current_client, price: get_cart_price(cart))
    cart.cart_goods.each do |cart_good|
      order.order_goods.push(OrderGood.new(order: order, rest: cart_good.rest))
    end
    if order.save
      cart.cart_goods.destroy_all
      flash[:success] = "Заказ успешно создан"
      FeedbackMailer.order_create_email(order).deliver
      redirect_to root_path and return
    else
      flast[:errors] = order.errors.messages
      redirect_to cart_path and return
    end
  end
end