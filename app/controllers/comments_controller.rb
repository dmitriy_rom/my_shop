class CommentsController < ApplicationController
  def create
    if comment = Comment.create!(client: current_client, good_id: params[:good_id], text: params[:text], rating: params[:rating])
      puts comment.errors.messages
      render json: { success: true, message: "Комментарий успешно добавлен" }
    else
      puts comment.errors.messages
      render json: { success: false, message: "Произошла ошибка. Повторите попытку позже." }
    end
  end
end