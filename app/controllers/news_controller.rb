class NewsController < ApplicationController
  def index
    @news = News.all.order(:created_at)
  end

  def show
    @news = News.find_by(id: params[:id])
  end
end
