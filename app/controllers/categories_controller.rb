class CategoriesController < ApplicationController
  def show
    @category = Category.find_by(slug: params[:slug])
    raise ActionController::RoutingError.new('Not Found') unless @category.present?
    @goods = @category.goods
  end
end
