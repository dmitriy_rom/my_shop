document.addEventListener('DOMContentLoaded', function(event) {
    if(document.querySelector('select#rest')) {
        handleRestChange(document.querySelector('select#rest'));
    }
    if(document.getElementById('createComment')) {
        handleCommentCreate();
    }
    handleToggleGood();
})

const handleRestChange = (select) => {
  select.addEventListener('change', function(ev) {
      ev.preventDefault();
      sendRestChangeRequest(ev.target.value);
  })
}

async function sendRestChangeRequest(id) {
    let response = await fetch(`/rest_change/${id}`);
    if (response.ok) {
        let json = await response.json();
        if(json.success) {
            document.querySelector('.good-card__cost-value').value = json.cost;
            document.querySelector('.js-good-toggle').value = json.in_cart ? 'Удалить' : 'В корзину'
        }
    } else {
        alert("Ошибка HTTP: " + response.status);
    }
}

function handleToggleGood() {
    for(let form of document.querySelectorAll('.toggleGood')) {
        console.log(form);
        toggleGood(form);
    }
}

async function toggleGood(form) {
    form.onsubmit = async (e) => {
        e.preventDefault();
        e.stopPropagation();
        fetch('/toggle_good', {
            method: 'POST',
            body: new FormData(form)
        })
            .then(function(response) {
                if(response.ok) {
                    return response.json();
                }
                throw new Error('error');
            })
            .then(function(data) {
                if(data.success) {
                    if(document.querySelector('.cart__cart-good')) {
                        form.closest('.cart__cart-good').style.display = 'none';
                        document.querySelector('.cart__cart-price').textContent = data.cart_price
                    } else {
                        document.querySelector('.js-good-toggle').value = data.in_cart ? 'Удалить' : 'В корзину';
                    }
                } else {
                    alert('Произошла ошибка. Пожалуйста, повторите попытку позже')
                }
            })
            .catch(function(error) {
                alert('Произошла ошибка. Пожалуйста, повторите попытку позже')
            })
    }
}

async function handleCommentCreate() {
    createComment.onsubmit = async (e) => {
        e.preventDefault();
        e.stopPropagation();
        fetch(createComment.action, {
            method: 'POST',
            body: new FormData(createComment)
        })
            .then(function(response) {
                if(response.ok) {
                    return response.json();
                }
                throw new Error('error');
            })
            .then(function(data) {
                if(data.success) {
                    document.querySelector('.error-hint').classList.add('hidden');
                    createComment.classList.add('hidden');
                    document.querySelector('.success-hint').textContent = data.message;
                } else {
                    document.querySelector('.error-hint').textContent = data.message
                }
            })
            .catch(function(error) {
                alert('Произошла ошибка. Пожалуйста, повторите попытку позже')
            })
    }
}