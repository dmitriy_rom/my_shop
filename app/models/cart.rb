class Cart < ApplicationRecord
  belongs_to :client, optional: true
  has_many :cart_goods, dependent: :delete_all

  validates :makeshift_id, presence: true, if: ->(cart){ cart.client.blank? }
  validates :client, presence: true, if: ->(cart){ cart.makeshift_id.blank? }

  def self.find_and_update(makeshift_id, client)
    cart = Cart.find_by(makeshift_id: makeshift_id)
    cart.update(client: client) if cart.present?
    return cart
  end

end
