class GoodImage < ApplicationRecord
  belongs_to :good
  validates :image, :good, presence: true
  mount_uploader :image, GoodImageUploader
end
