class Good < ApplicationRecord
  has_and_belongs_to_many :categories
  has_and_belongs_to_many :good_parameter_variants
  has_many :rests, dependent: :destroy
  has_many :good_images, dependent: :destroy
  has_many :comments, dependent: :destroy

  accepts_nested_attributes_for :good_images, :allow_destroy => true
  accepts_nested_attributes_for :rests, :allow_destroy => true

  validates :title, :slug, :cost, :categories, :prior, presence: true
  validates :slug, uniqueness: true

  before_validation :prepare_slug

  scope :ordered, -> { order(prior: :desc) }

  def prepare_slug
    unless self.slug.present?
      slug = UrlStringPreparator.slug(self.slug, self.title)
      if Good.find_by(slug: slug).present?
        self.slug = slug + "-#{Good.where("slug like '#{slug}%'").count}"
      else
        self.slug = slug
      end
    end
  end

  def in_stock?
    Rest.where('good_id = ? and rest >= ?', self.id, 1).any?
  end

  def collect_rests
    self.rests.reduce([]) { |arr, rest| arr.push([rest.collect_config, rest.id]) }
  end
end
