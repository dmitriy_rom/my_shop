class News < ApplicationRecord
  validates :title, :image, :text, presence: true
  mount_uploader :image, GoodImageUploader
end
