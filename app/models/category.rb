class Category < ApplicationRecord
  has_and_belongs_to_many :goods

  validates :title, :slug, :prior, presence: true
  validates :slug, uniqueness: true

  scope :ordered, -> { order(prior: :desc) }
end
