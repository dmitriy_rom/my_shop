class GoodParameterVariant < ApplicationRecord
  has_and_belongs_to_many :goods
  has_and_belongs_to_many :rests
  belongs_to :good_parameter

  validates :title, :good_parameter, presence: true
  validates :title, uniqueness: { scope: [:good_parameter] }
end
