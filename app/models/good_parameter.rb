class GoodParameter < ApplicationRecord
  has_many :good_parameter_variants

  validates :title, presence: true
  validates :title, uniqueness: true
end
