class Rest < ApplicationRecord
  belongs_to :good
  has_and_belongs_to_many :good_parameter_variants
  has_many :cart_goods, dependent: :destroy
  accepts_nested_attributes_for :good_parameter_variants, :allow_destroy => true

  validates :rest, :good, :cost, presence: true

  def collect_config
    self.good_parameter_variants.map { |gpv| "#{gpv.good_parameter.title}: #{gpv.title}" }.join(", ")
  end
end
