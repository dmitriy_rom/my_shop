class CartGood < ApplicationRecord
  belongs_to :cart
  belongs_to :rest

  validates :count, :max_count, :cart, :rest, presence: true
end
