class Comment < ApplicationRecord
  belongs_to :client
  belongs_to :good

  validates :text, :client, :good, presence: true
end
