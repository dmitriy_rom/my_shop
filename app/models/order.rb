class Order < ApplicationRecord
  belongs_to :client
  has_many :order_goods, dependent: :destroy
  validates :client, :status, :price, presence: true
end
