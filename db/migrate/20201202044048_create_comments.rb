class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.text :text, null: false
      t.integer :rating, default: 5
      t.belongs_to :client
      t.belongs_to :good

      t.timestamps
    end
  end
end
