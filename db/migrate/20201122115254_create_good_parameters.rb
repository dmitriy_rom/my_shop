class CreateGoodParameters < ActiveRecord::Migration[5.2]
  def change
    create_table :good_parameters do |t|
      t.string :title, null: false

      t.timestamps
    end
  end
end
