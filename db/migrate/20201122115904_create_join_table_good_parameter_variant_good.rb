class CreateJoinTableGoodParameterVariantGood < ActiveRecord::Migration[5.2]
  def change
    create_join_table :good_parameter_variants, :goods do |t|
      # t.show [:good_parameter_variant_id, :good_id]
      # t.show [:good_id, :good_parameter_variant_id]
    end
  end
end
