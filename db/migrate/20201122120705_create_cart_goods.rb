class CreateCartGoods < ActiveRecord::Migration[5.2]
  def change
    create_table :cart_goods do |t|
      t.integer :count, null: false
      t.integer :max_count

      t.belongs_to :cart
      t.belongs_to :rest

      t.timestamps
    end
  end
end
