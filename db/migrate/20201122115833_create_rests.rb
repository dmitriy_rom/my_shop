class CreateRests < ActiveRecord::Migration[5.2]
  def change
    create_table :rests do |t|
      t.integer :rest
      t.integer :cost

      t.belongs_to :good

      t.timestamps
    end
  end
end
