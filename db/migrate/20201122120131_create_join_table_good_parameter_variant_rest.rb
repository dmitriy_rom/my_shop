class CreateJoinTableGoodParameterVariantRest < ActiveRecord::Migration[5.2]
  def change
    create_join_table :good_parameter_variants, :rests do |t|
      # t.show [:good_parameter_variant_id, :rest_id]
      # t.show [:rest_id, :good_parameter_variant_id]
    end
  end
end
