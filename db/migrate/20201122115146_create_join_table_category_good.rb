class CreateJoinTableCategoryGood < ActiveRecord::Migration[5.2]
  def change
    create_join_table :categories, :goods do |t|
      # t.show [:category_id, :good_id]
      # t.show [:good_id, :category_id]
    end
  end
end
