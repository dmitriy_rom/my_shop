class CreateGoodParameterVariants < ActiveRecord::Migration[5.2]
  def change
    create_table :good_parameter_variants do |t|
      t.string :title
      t.belongs_to :good_parameter

      t.timestamps
    end
  end
end
