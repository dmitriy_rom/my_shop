class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.integer :status, null: false, default: 0
      t.integer :price, null: false
      t.string :delivery_address

      t.belongs_to :client

      t.timestamps
    end
  end
end
