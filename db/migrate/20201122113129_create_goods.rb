class CreateGoods < ActiveRecord::Migration[5.2]
  def change
    create_table :goods do |t|
      t.string :title, null: false
      t.string :description, null: false
      t.string :slug, null: false
      t.integer :cost, null: false
      t.integer :prior, default: 0


      t.timestamps
    end
  end
end
