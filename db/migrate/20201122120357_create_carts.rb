class CreateCarts < ActiveRecord::Migration[5.2]
  def change
    create_table :carts do |t|
      t.string :makeshift_id
      t.belongs_to :client

      t.timestamps
    end
  end
end
