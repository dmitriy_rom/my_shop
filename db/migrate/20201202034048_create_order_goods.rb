class CreateOrderGoods < ActiveRecord::Migration[5.2]
  def change
    create_table :order_goods do |t|
      t.belongs_to :rest
      t.belongs_to :order

      t.timestamps
    end
  end
end
