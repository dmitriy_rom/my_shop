class CreateCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :categories do |t|
      t.string :title, null: false
      t.string :slug, null: false
      t.integer :prior, null: false, default: 0

      t.timestamps
    end
  end
end
