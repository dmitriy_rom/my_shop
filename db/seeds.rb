def seedfile(fname)
  File.open File.join(Rails.root, "public/content/seeds/", fname)
end

AdminUser.create(email: "admin@example.com", password: "password")

puts "Create categories"
Category.create(title: "Ноутбуки", slug: "notebooks")
Category.create(title: "Моноблоки", slug: "monoblocks")
Category.create(title: "Системные блоки", slug: "system units")
Category.create(title: "Микрокомпьютеры", slug: "microcomputers")

puts "Craete parameters"
GoodParameter.create(title: "ОЗУ")
GoodParameter.create(title: "ПЗУ")
GoodParameter.create(title: "ОС")

puts "Create goods"
good = Good.create(title: '15.6 Ноутбук Acer Aspire 3 A315-22-48FX черный',
            description: 'Это не просто ноутбук. Это ваш партнер по работе, который будет помогать вам в течение всего дня, куда бы вы ни отправились. ',
            cost: 30000,
            categories: [Category.all[0]],
            good_images: [GoodImage.create(image: seedfile("acer_aspire.jpg"))]
)
puts good.errors.messages
Good.create(title: 'Микрокомпьютер Cubieboard 1',
            description: 'Микрокомпьютер Cubieboard 1 - платформа для усовершенствования и создания различных устройств, при этом не уступает по функционалу полноценному компьютеру',
            cost: 1500,
            categories: [Category.all[3]],
            good_images: [GoodImage.create(image: seedfile("Cubieboard.jpg"))]
)
Good.create(title: 'Моноблок Lenovo IdeaCentre A340-24IGM',
            description: 'Моноблок Lenovo IdeaCentre A340-24IGM − стильный аппарат с надежными составляющими, который выполнен в виде монитора со встроенными аппаратными составляющими.',
            cost: 29000,
            categories: [Category.all[1]],
            good_images: [GoodImage.create(image: seedfile("ideaCentre.jpg"))]
)
Good.create(title: 'ПК DEXP Aquilon O237',
            description: 'ПК DEXP Aquilon O237 представлен в форм-факторе Mini-Tower и имеет классическую черную расцветку корпуса.',
            cost: 13000,
            categories: [Category.all[2]],
            good_images: [GoodImage.create(image: seedfile("aquilon.jpg"))]
)
Good.create(title: 'ПК Dell Vostro 3681 [3681-9955]',
            description: 'Настольные компьютеры Vostro 3681 повышают производительность и предоставляют широкие возможности расширения благодаря процессорам 10-го поколения Intel Core и до 32 Гбайт памяти DDR4 в более компактном корпусе.',
            cost: 42000,
            categories: [Category.all[2]],
            good_images: [GoodImage.create(image: seedfile("dexp.jpg"))]
)
Good.create(title: 'Ультрабук Acer Swift 1 SF114-32-P6XL серебристый',
            description: 'С ультрабуком Acer Swift 1 SF114-32-P6XL вам открывается множество технических возможностей, связанных с обыкновенной работой за компьютером. ',
            cost: 35000,
            categories: [Category.all[0]],
            good_images: [GoodImage.create(image: seedfile("swift.jpg"))]
)
Good.create(title: 'Ноутбук ASUS VivoBook D712DA-AU169T серебристый',
            description: 'Этот ноутбук создан для тех, кто хочет получить надежное и производительное компьютерное устройство с наиболее востребованным функционалом.',
            cost: 45000,
            categories: [Category.all[0]],
            good_images: [GoodImage.create(image: seedfile("vivobook.jpg"))]
)
Good.create(title: 'Микрокомпьютер ORANGE PI LITE',
            description: 'Микрокомпьютер ORANGE PI LITE на плате размером всего 69x48 мм может послужить основой для создания оригинального оборудования. ',
            cost: 2300,
            categories: [Category.all[3]],
            good_images: [GoodImage.create(image: seedfile("orangepi.jpg"))]
)
Good.create(title: 'Моноблок HP 22-c0121ur AiO 7JT36EA',
            description: 'Моноблок HP 22-c0121ur AiO 7JT36EA приятно удивит своего обладателя расширенной акустической системой, при помощи которой достигается беспрецедентное качество звучания как высоких, так и низких частот. ',
            cost: 67000,
            categories: [Category.all[1]],
            good_images: [GoodImage.create(image: seedfile("hp_monoblock.jpg"))]
)
Good.create(title: 'Моноблок Apple iMac',
            description: 'Моноблок Apple iMac MHK03RU/A представлен в стильном серебристом дизайне, оснащен алюминиевым корпусом, устойчивой подставкой и IPS-краном 21.5” с разрешением 1920x1080. ',
            cost: 107000,
            categories: [Category.all[1]],
            good_images: [GoodImage.create(image: seedfile("monoblock_apple.jpg"))]
)
Good.create(title: 'Моноблок Apple iMac 27 Retina 5K [MXWV2RU/A]',
            description: 'Всё в одном. Всё под силу. Только представьте, с помощью Apple iMac Retina 5K можно воплотить абсолютно всё. У него превосходный дизайн. Им очень удобно пользоваться. ',
            cost: 242000,
            categories: [Category.all[1]],
            good_images: [GoodImage.create(image: seedfile("apple_retina.jpg"))]
)
Good.create(title: 'ПК Asus D500SA-310100032R',
            description: 'ASUS ExpertCenter D500SA - персональный компьютер Small форм-фактора, предоставляющий исключительную ценность для малого бизнеса. ASUS ExpertCenter D500SA обладает мощной, надежной и расширяемой производительностью.',
            cost: 50000,
            categories: [Category.all[2]],
            good_images: [GoodImage.create(image: seedfile("asus_pg.jpg"))]
)
Good.create(title: 'ПК ASUS D500SA-510400039R',
            description: 'ASUS ExpertCenter D500SA - персональный компьютер Small форм-фактора, предоставляющий исключительную ценность для малого бизнеса. ASUS ExpertCenter D500SA обладает мощной, надежной и расширяемой производительностью.',
            cost: 55000,
            categories: [Category.all[2]],
            good_images: [GoodImage.create(image: seedfile("asus_pk.jpg"))]
)
Good.create(title: 'ПК DEXP Mars E272',
            description: 'ПК DEXP Mars E272 – имеющий сбалансированную конфигурацию персональный компьютер, который обладает компактными размерами.',
            cost: 57000,
            categories: [Category.all[2]],
            good_images: [GoodImage.create(image: seedfile("dexp_pk.jpg"))]
)
Good.create(title: 'Ноутбук MSI Modern 15 A10M-407RU черный',
            description: 'Этот ноутбук создан для тех, кто хочет получить надежное и производительное компьютерное устройство с наиболее востребованным функционалом.',
            cost: 57000,
            categories: [Category.all[0]],
            good_images: [GoodImage.create(image: seedfile("msi.jpg"))]
)
Good.create(title: 'Ультрабук Honor MagicBook NblL-WDQ9HN',
            description: 'Ультрабук Honor MagicBook NblL-WDQ9HN подходит для работы и увлекательного досуга за мультимедийными развлечениями. Благодаря металлическому корпусу с компактными размерами и весом 1.38 кг вы сможете удобно носить ультрабук с собой каждый день. ',
            cost: 60000,
            categories: [Category.all[0]],
            good_images: [GoodImage.create(image: seedfile("honor.jpg"))]
)
Good.create(title: 'Микрокомпьютер ORANGE PI PC Plus',
            description: 'Микрокомпьютер ORANGE PI PC Plus – это усовершенствованный вариант самой популярной в линейке Orange Pi модели (Pi PC). Отличается от прообраза наличием встроенной памяти EMMC Flash (8 ГБ) и WiFi-модуля (Realteck 8189). ',
            cost: 4000,
            categories: [Category.all[3]],
            good_images: [GoodImage.create(image: seedfile("orange_pi.jpg"))]
)
Good.create(title: 'Микрокомпьютер Raspberry Pi 3 Model B+',
            description: 'Микрокомпьютер Raspberry Pi 3 Model B+ представляет собой функциональную малогабаритную сборку, оптимальную для построения автоматизированных систем, сверхкомпактных настольных системных блоков и т.д. Изделие представляет собой материнскую плату, оборудованную процессором Broadcom 4x1400 МГц и 1 ГБ оперативной памяти. ',
            cost: 4000,
            categories: [Category.all[3]],
            good_images: [GoodImage.create(image: seedfile("raspberry.jpg"))]
)

# 0 - OZU, 1 - PZU, 2 - OS
puts "Create rests"

Rest.create(good: Good.all[0], rest: 2, cost: 30000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "8гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "256гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Без ОС", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[0], rest: 2, cost: 35000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "16гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "256гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Без ОС", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[0], rest: 1, cost: 32000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "8гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "256гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Windows", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[1], rest: 2, cost: 1500)
Rest.create(good: Good.all[2], rest: 1, cost: 29000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "8гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "128гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Без ОС", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[2], rest: 5, cost: 38000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "16гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "256гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Linux", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[3], rest: 3, cost: 13000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "4гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "128гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Windows", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[3], rest: 3, cost: 15000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "8гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "128гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Windows", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[4], rest: 4, cost: 42000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "16гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "256гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Без ОС", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[4], rest: 3, cost: 48000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "16гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "256гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Windows", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[5], rest: 2, cost: 35000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "8гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "256гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Без ОС", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[5], rest: 3, cost: 45000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "16гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "256гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Windows", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[6], rest: 3, cost: 45000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "16гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "256гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Linux", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[6], rest: 1, cost: 50000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "16гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "256гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Windows", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[6], rest: 1, cost: 46000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "16гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "512гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Без ОС", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[7], rest: 3, cost: 2300)
Rest.create(good: Good.all[8], rest: 1, cost: 67000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "32гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "512гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Без ОС", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[8], rest: 1, cost: 70000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "32гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "512гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Windows", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[9], rest: 1, cost: 107000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "32гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "512гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Mac OS", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[10], rest: 1, cost: 242000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "64гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "1024гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Mac OS", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[11], rest: 1, cost: 50000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "16гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "256гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Windows", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[11], rest: 1, cost: 50000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "16гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "256гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Windows", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[11], rest: 1, cost: 50000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "16гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "512гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Без ОС", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[12], rest: 1, cost: 55000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "16гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "512гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Windows", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[13], rest: 1, cost: 57000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "16гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "512гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Windows", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[13], rest: 1, cost: 60000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "32гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "512гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Windows", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[14], rest: 1, cost: 60000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "8гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "256гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Без ОС", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[14], rest: 1, cost: 60000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "8гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "256гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Linux", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[14], rest: 1, cost: 65000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "8гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "256гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Windows", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[14], rest: 1, cost: 65000, good_parameter_variants: [
    GoodParameterVariant.find_or_create_by(title: "16гб", good_parameter: GoodParameter.all[0]),
    GoodParameterVariant.find_or_create_by(title: "256гб", good_parameter: GoodParameter.all[1]),
    GoodParameterVariant.find_or_create_by(title: "Без ОС", good_parameter: GoodParameter.all[2])
])
Rest.create(good: Good.all[15], rest: 1, cost: 4000)
Rest.create(good: Good.all[16], rest: 1, cost: 4000)


puts "====== Create news ======="
News.create(title: "Акция! Скидка на все ноутбуки 10%", image: seedfile("notebook_sale.jpg"), text:
  "Вы можете совершить по-настоящему выгодную покупку, получив скидку на качественный товар до 10% от его
реальной рыночной стоимости. Все размещенные в на сайте ноутбуки со скидкой - новые, имеют полноценную
гарантию производителя. Выберите понравившийся Вам вариант и позвоните нашим менеджерам, чтобы уточнить
возможность его приобретения.")

News.create(title: "Закрытие магазинов 1 янвярая", image: seedfile("closed_pic.jpg"), text:
  "Уважаемые покупатели! Уведомляем Вас о том, что, в связи с новогодними праздниками, наш магазин не будет работать
1 января. Приносим свои извинения за неудобства. Начиная со 2 января магазин будет работать в обычном режиме.
Счастливых Вам каникул!")