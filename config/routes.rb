Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :clients
  get '/categories/:slug', to: 'categories#show', as: :category
  get '/goods/:slug', to: 'goods#show', as: :good
  post '/toggle_good', to: 'carts#toggle_good', as: :toggle_good
  get 'rest_change/:id', to: 'carts#rest_change', as: :rest_change
  get '/cart', to: 'carts#show', as: :cart
  post 'create_order', to: 'orders#create', as: :create_order
  post 'create_comment', to: 'comments#create', as: :create_comment
  get '/news', to: 'news#index', as: :news
  get '/news/:id', to: 'news#show', as: :news_card
  root 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
